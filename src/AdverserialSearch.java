import java.util.InputMismatchException;
import java.util.Scanner;

// this is the top-level code that allows you to call minimaxDecision 
// for a given state to determine the best move

public class AdverserialSearch {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CheckersState n = new CheckersState();
		MiniMaxSearch solver=new MiniMaxSearch();

		//while (!(n.isTerminal(1))){
			Move s=solver.minimaxDecision(n);
			System.out.println(s);
		//}
		
	}

}
