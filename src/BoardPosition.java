
/**
 * A simple class to represent cells of the checkers board in terms of (row, col).
 * Useful for passing around list of board cells (such as a sequence of jumps).
 * @author Joey
 *
 */
public class BoardPosition {
	
	public int row;
	public int col;
	
	public BoardPosition(int r, int c){
		row = r;
		col = c;
	}
	
	public BoardPosition(BoardPosition b){
		row = b.row;
		col = b.col;
	}
}
