// RmCheckersClient.java is a client that interacts with Sam, a checkers 
// server. It is designed to illustrate how to communicate with the server
// in a minimal way.  It is not meant to be beautiful java code.
// Given the correct machine name and port for the server, a user id, and a 
// password (_machine, _port, _user, and _password in the code), running 
// this program will initiate connection and start a game with the default 
// player. (the _machine and _port values used should be correct, but check
// the protocol document.)
// 
// the program has been tested and used under Java 5.0 and 6.0, but probably 
// would work under older or newer versions.
//
// Copyright (C) 2008 Robert McCartney

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CheckersClient {
	
	// this client is designed to play against a server, but the code could easily
	// be modified to play against something else.
	// need legit id here
	// need password here
	private final static String _opponent = "0";
	// need machine URL here
	// need port number here
	private Socket _socket = null;
	private PrintWriter _out = null;
	private BufferedReader _in = null;

	private String _gameID;
	private String _myColor;
	
	private CheckersPlayer _player = null; 

	public CheckersClient(){	
		_socket = openSocket();
	}
	
	public String getNextMove(String opponentMove){
		return _player.getNextMove(opponentMove); //TODO: get the next move from CheckersState/miniMax
	}
	
	public boolean gameOver(){
		return false; //TODO: implement real check via CheckersState
	}
	
	public void gameResult(String resultMessage){
		boolean gameWon = false;
		if(resultMessage.contains("Black")){
			if(getColor().equals("White")){
				gameWon = false;
			}else{
				gameWon = true;
			}
		}else{
			if(getColor().equals("White")){
				gameWon = true;
			}else{
				gameWon = false;
			}
		}
		if(gameWon){
			System.out.println("Game over. We won.");
		}else{
			System.out.println("Game over. We lost.");
		}
		
	}

	public Socket getSocket(){
		return _socket;
	}

	public PrintWriter getOut(){
		return _out;
	}

	public BufferedReader getIn(){
		return _in;
	}

	public void setGameID(String id){
		_gameID = id;
	}

	public String getGameID() {
		return _gameID;
	}

	public void setColor(String color){
		_myColor = color;
	}

	public String getColor() {
		return _myColor;
	}

	public static void main(String[] argv){
		@SuppressWarnings("unused")
		String readMessage;
		CheckersClient myClient = new CheckersClient();

		try{
			myClient.readAndEcho(); // start message
			myClient.readAndEcho(); // ID query
			myClient.writeMessageAndEcho(_user); // user ID

			myClient.readAndEcho(); // password query 
			myClient.writeMessage(_password);  // password

			myClient.readAndEcho(); // opponent query
			myClient.writeMessageAndEcho(_opponent);  // opponent

			myClient.setGameID(myClient.readAndEcho().substring(5,9)); // game 
			myClient.setColor(myClient.readAndEcho().substring(6,11));  // color
			System.out.println("I am playing as "+myClient.getColor()+ " in game number "+ myClient.getGameID());
			
			// depends on color--a black move if i am white, Move:Black:i:j
			// otherwise a query to move, ?Move(time):
			if (myClient.getColor().equals("White")) {
				myClient._player = new MinimaxPlayer('w');
				while(!myClient.gameOver()){
					readMessage = myClient.readAndEcho();  // black move (opponent)
					if(readMessage.contains("Result:")){
						myClient.gameResult(readMessage);
						break;
					}
					String myMove = myClient.getNextMove(readMessage);
					myClient.readAndEcho(); //?Move prompt
					myClient.writeMessageAndEcho(myMove); //sent move
					readMessage = myClient.readAndEcho();  // white move (me)
				}
			}
			else {
				myClient._player = new MinimaxPlayer('b');
				String myMove = myClient._player.getFirstMove();
				myClient.readAndEcho(); //?Move prompt
				myClient.writeMessageAndEcho(myMove);
				while(!myClient.gameOver()){
					readMessage = myClient.readAndEcho();  // black move (me)
					readMessage = myClient.readAndEcho();  // white move (opponent)
					if(readMessage.contains("Result:")){
						myClient.gameResult(readMessage);
						break;
					}
					myMove = myClient.getNextMove(readMessage);
					myClient.readAndEcho(); //?Move prompt
					myClient.writeMessageAndEcho(myMove);
				}
			}

			myClient.getSocket().close();
		} catch  (IOException e) {
			System.out.println("Failed in read/close");
			System.exit(1);
		}
	}

	public String readAndEcho() throws IOException
	{
		String readMessage = _in.readLine();
		System.out.println("read: "+readMessage);
		return readMessage;
	}

	public void writeMessage(String message) throws IOException
	{
		_out.print(message+"\r\n");  
		_out.flush();
	}

	public void writeMessageAndEcho(String message) throws IOException
	{
		_out.print(message+"\r\n");  
		_out.flush();
		System.out.println("sent: "+ message);
	}

	public  Socket openSocket(){
		//Create socket connection, adapted from Sun example
		try{
			_socket = new Socket(_machine, _port);
			_out = new PrintWriter(_socket.getOutputStream(), true);
			_in = new BufferedReader(new InputStreamReader(_socket.getInputStream()));
		} catch (UnknownHostException e) {
			System.out.println("Unknown host: " + _machine);
			System.exit(1);
		} catch  (IOException e) {
			System.out.println("No I/O");
			System.exit(1);
		}
		return _socket;
	}
}