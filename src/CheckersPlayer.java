import java.util.List;


public interface CheckersPlayer {
	String getNextMove(String opponentMove);
	String getFirstMove();
}
