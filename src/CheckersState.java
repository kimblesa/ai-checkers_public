import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.awt.Point;

public class CheckersState {
	// list from 1 to 35: 
	/* [ 1, 2, 3, 4, 5, 6, 7, 8,
	 *  10,11,12,13,14,15,16,17,
	 *  19,20,21,22,23,24,25,26,
	 *  28,29,30,31,32,33,34,35 ]
	 *  9, 18, and 27 are not used for internal calculations.
	 */
	private Map<Integer, Character> _board = new HashMap<Integer, Character>(32);
	private List<Move> _actionList = new LinkedList<Move>();
	// complete move list, with jumps and regular moves, needed for evaluation poly.
	private List<Move> _completeList = new LinkedList<Move>();
	private Character _currentPlayer;
	private int _min = 1, _max = 35;
	private int _boardSize = 8;

	/* Initialize board to starting positions.
	 * 
	 */
	public CheckersState() {
		_board = baseBoard();
		//\TODO who should the starting player be? does it matter?
		_currentPlayer = 'b';
	}

	/* Initialize board to a given new internal board. */
	public CheckersState(Map<Integer, Character> board, Character player) {
		_board = board;
		_currentPlayer = player;
	}

	/* Initialize board to a given server representation board. */
	public CheckersState(char[][] board, Character player) {
		_board = stoi(board);
		_currentPlayer = player;
	}
	
	/* returns the hash map representation of the board. This function was used for testing */
	Map<Integer,Character> getHash(){
		return _board;
	}

	/* 
	 * Generates a list of all possible moves based on the current player
	 */
	List<Move> actions() {
		// make sure list is clear.
		_actionList.clear();
		LinkedList<Move> temp = new LinkedList<Move>();
		
		for (Map.Entry<Integer, Character> entry : _board.entrySet()) {
			if (Character.toLowerCase(entry.getValue())==_currentPlayer ){
				// calls actionAt to return a list of move this piece can take
				temp.addAll(actionsAt(new Move(entry.getValue(), entry.getKey(), entry.getKey())));
			}
		}
		// For every move added in the list, filter out the jump moves
		for (Move m: temp){
			if (m.jDirection().length()>0){
				_actionList.add(m);
			}
		}
		
		// returns the actionList of jump moves  
		if (!(_actionList.isEmpty())){
			_completeList.addAll(_actionList);
			return _actionList;
		}
		else{
			_completeList.addAll(temp);
			_actionList = temp;
			return temp;
		}
	}

	/* Recursive method that generates moves for all pawns and kings
	 * TODO : This code works however its too long and extraneous. Need to fix this if I can 
	 * Suggestions: Instead of concatenating strings we should concatenate key values i.e. 
	 * "152025". Instead of wastefully having code duplication for kings to move backwards,
	 * look into having king pieces pretending to be a opposing pawn piece to allow it to move backwards.
	 * Look into cycle detection as well. Handle code duplication    
	 */
	List<Move> actionsAt(Move base) {
		
		List<Move> tempActions = new LinkedList<Move>();
		// Get the initial key value
		int temp = base.end();
		boolean shortcircuit = false; // initialize detection for jumps
		int left=5, right=4;
		int tempLeft, tempRight, tempLowerLeft, tempLowerRight;
		char opposingPlayer=' ';
		
		// Set the direction depending on what color piece they are. Refer to Samuel's representation
		if (_currentPlayer == 'b') {
			tempLeft = temp + left;
			tempRight = temp + right;
			tempLowerLeft= temp - right;
			tempLowerRight= temp - left;
			opposingPlayer = 'w';
		} else {
			left=-5;
			right=-4;
			tempLeft = temp + left;
			tempRight = temp + right;
			tempLowerLeft= temp - right;
			tempLowerRight= temp - left;
			opposingPlayer = 'b';
		} 
		
		// Insurance when detecting cycles in the possible actions
		try{
			
			/*
			 * Terminating conditions for the pawn case. When pawn's left and right directions are not valid meaning the
			 * pawn is at the end of the board then terminate and add the move to tempActions
			 */
			if ( ( (!_board.containsKey(tempLeft+left) && (!_board.containsKey(tempRight+right)) ) && base.start() != base.end()  ) ){
				tempActions.add(base);
			}
			// terminating condition for kings case checks all directions and see if they are valid. 
			else if( (Character.toUpperCase(base.player())==base.player())
					&&( (!_board.containsKey(tempLowerLeft-right) || !_board.containsKey(tempLowerRight-left))
							|| (!_board.containsKey(tempLeft+left) || !_board.containsKey(tempRight+right)) ) 
							&& base.start() != base.end() ){
				tempActions.add(base);
			}
			else {// if it doesn't terminate then it can still move  boolean shortcircuit is still false.....
				/*
				 * This is for the upper left case. Checks the upper left case if it can jump then make the jump.
				 * Performs recursive function.	shortcircuit will be set true for this iteration.
				 */
				if (_board.containsKey(tempLeft) && Character.toLowerCase(_board.get(tempLeft))== opposingPlayer){
					if (_board.containsKey(tempLeft+left) && _board.get(tempLeft+left)== ' ' && (!base.lastMove().equals("lr")) ){
						tempActions.addAll(actionsAt(new Move(base.player(),base.start(), tempLeft+left, base.jDirection()+"ul")) );
						shortcircuit=true;
					}
				}// if it can't jump then checks if there is an empty space. If there is move in that direction
				else if (_board.containsKey(tempLeft) && _board.get(tempLeft)==' ' && base.jDirection().length()==0 )
					tempActions.add(new Move(base.player(),base.start(), tempLeft) );

				/*
				 * This is for the upper right case. Checks the upper right case if it can jump then make the jump.
				 * Performs recursive function.	shortcircuit will be set true for this iteration.
				 */
				if (_board.containsKey(tempRight) && Character.toLowerCase(_board.get(tempRight))== opposingPlayer){
					if (_board.containsKey(tempRight+right) && _board.get(tempRight+right)==' ' && (!base.lastMove().equals("ll")) ){
						tempActions.addAll(actionsAt(new Move(base.player(),base.start(), tempRight+right, base.jDirection()+"ur")) );
						shortcircuit=true;
					}
				}// if it can't jump then checks if there is an empty space. If there is move in that direction
				else if (_board.containsKey(tempRight) && _board.get(tempRight)==' ' && base.jDirection().length()==0 )
					tempActions.add(new Move(base.player(),base.start(), tempRight) );

				/*
				 * This is for the lower left case. Checks the lower left case. if it can jump and if its a king then make the jump.
				 * Performs recursive function.	shortcircuit will be set true for this iteration.
				 */
				if (_board.containsKey(tempLowerLeft) && Character.toLowerCase(_board.get(tempLowerLeft))== opposingPlayer && base.player() == Character.toUpperCase(base.player()) ){
					if (_board.containsKey(tempLowerLeft-right) && _board.get(tempLowerLeft-right)==' '  && (!base.lastMove().equals("ur")) ){
						tempActions.addAll(actionsAt(new Move(base.player(),base.start(), tempLowerLeft-right, base.jDirection()+"ll")) );
						shortcircuit = true;
					} 
				}// if it can't jump then checks if there is an empty space. If there is move in that direction
				else if (_board.containsKey(tempLowerLeft) && _board.get(tempLowerLeft)==' ' && base.jDirection().length()==0 && base.player() == Character.toUpperCase(base.player()) )
					tempActions.add(new Move(base.player(),base.start(), tempLowerLeft) );

				/*
				 * This is for the lower right case. Checks the lower right case. if it can jump and if its a king then make the jump.
				 * Performs recursive function.	shortcircuit will be set true for this iteration.
				 */
				if (_board.containsKey(tempLowerRight) && Character.toLowerCase(_board.get(tempLowerRight))== opposingPlayer && base.player() == Character.toUpperCase(base.player()) ){
					if (_board.containsKey(tempLowerRight-left) && _board.get(tempLowerRight-left)==' '  && (!base.lastMove().equals("ul")) ){
						tempActions.addAll(actionsAt(new Move(base.player(),base.start(), tempLowerRight-left, base.jDirection()+"lr")) );
						shortcircuit=true;
					}
				}// if it can't jump then checks if there is an empty space. If there is move in that direction
				else if (_board.containsKey(tempLowerRight) && _board.get(tempLowerRight)==' ' && base.jDirection().length()==0 && base.player() == Character.toUpperCase(base.player()) )
					tempActions.add(new Move(base.player(),base.start(), tempLowerRight) );
				
				// if short circuit is false and it at least moved then add move.
				if(base.start() != base.end() && shortcircuit == false ){
					tempActions.add(base);
				}

				return tempActions;

			}

		}
		catch (StackOverflowError e){ // if it does cycle then return the current list
			return tempActions;
		}

		return tempActions;	


	}

	/* return the result of a move. Should test whether the move is valid beforehand */
	/* TODO: This code is long and to extraneous but it works. We need a better way of 
	 * implementing this. Suggestion: Have the actions() convert integer key values from the
	 * hash map into strings i.e "152025" this would be a single jump grab the string values then remove and re populate
	 */
	CheckersState result(Move x) {
		Move temp=x;
		if (!(isValid(temp))){ // test if the move is Valid move. 
			return null;
		}
		
		String d= temp.jDirection(); // initialize the string Jump direction
		Map<Integer, Character> _tBoard=new HashMap<Integer, Character>(_board); // initialize a copy of the board
		char opPlayer; 
		Character name=_tBoard.get(temp.start()); // Get the name of the piece ex. 'w' or 'W'
		int left, right;
		
		/* Set the left and right values according to Samuel's Paper. Black moves down white goes up */
		if (_currentPlayer == 'w'){
			left = -5; 
			right= -4;
		}
		else {
			left = 5;
			right = 4;
		}

		while (!(d.length()==0)){ // checks if the piece can jump. If it can it has a string called jump direction
			String t=d.substring(0, 2);
			
			if (temp.start() == temp.end()){
				break;
			}
			
			if (t.equals("ul")){ // if the substring equals "ul" or upper left jump
				int s= temp.start(); // begin at start
				_tBoard.remove(s); // deletes the key from the hashmap
				_tBoard.put(s, ' '); // adds an empty space

				int tLeft= s+left; // moves upward left 
				_tBoard.remove(tLeft); // deletes the key in the hashmap
				_tBoard.put(tLeft, ' '); // puts a space

				tLeft = tLeft+left; // moves upward left 
				_tBoard.remove(tLeft); // deletes the key from the hashmap 
				_tBoard.put(tLeft, ' '); // puts a space

				temp=new Move(temp.player(), tLeft, temp.end(), d); // change temp to have a new start and end

			}
			else if (t.equals("ur")){ // if the substring equals "ur" or upper right jump

				int s= temp.start(); // begin at start
				_tBoard.remove(s); // deletes the key from the hashmap
				_tBoard.put(s, ' '); // adds an empty space

				int tRight= s+right; // moves upwards right
				_tBoard.remove(tRight); // deletes the key from the hashmap
				_tBoard.put(tRight, ' '); // adds a empty space

				tRight = tRight+right; // moves upwards right
				_tBoard.remove(tRight); // deletes the key from the hashmap
				_tBoard.put(tRight, ' '); // adds an empty space

				temp=new Move(temp.player(), tRight, temp.end(), d); // change temp to have a new start and end

			}
			else if (t.equals("ll")){ // code does the similar things mentioned above. 

				int s= temp.start(); 
				_tBoard.remove(s); 
				_tBoard.put(s, ' ');

				int ttLeft= s-right;
				_tBoard.remove(ttLeft);
				_tBoard.put(ttLeft, ' ');

				ttLeft = ttLeft-right;
				_tBoard.remove(ttLeft);
				_tBoard.put(ttLeft, ' ');

				temp=new Move(temp.player(), ttLeft, temp.end(), d);
			}
			else if (t.equals("lr")){ // code does a similar thing as mentioned above

				int s= temp.start();
				_tBoard.remove(s);
				_tBoard.put(s, ' ');

				int ttRight= s-left;
				_tBoard.remove(ttRight);
				_tBoard.put(ttRight, ' ');

				ttRight = ttRight-left;
				_tBoard.remove(ttRight);
				_tBoard.put(ttRight, ' ');

				temp=new Move(temp.player(), ttRight, temp.end(), d);
			}
			d=d.replaceFirst(t, "");
		}
		
		// Applies to any pawn and kings case remove any stating positions
		int start = x.start();
		_tBoard.remove(start);
		// places a space t start and removes the destination of the end key.
		_tBoard.put(start, ' ');
		_tBoard.remove(temp.end());
		
		// if its a pawn and it reaches at the end of the opposite side then king the piece 
		if (name == 'w' && (temp.end()==1 || temp.end()==2 || temp.end()==3 || temp.end()==4  ) )
			_tBoard.put(temp.end(), 'W');
		else if (name == 'b' && (temp.end()==32 || temp.end()==33 || temp.end()==34 || temp.end()==35) ) 
			_tBoard.put(temp.end(), 'B');
		else
			_tBoard.put(temp.end(), name); // else place the piece to its destination
		
		/* Switches the players */
		if (_currentPlayer.equals('w') )
			opPlayer='b';
		else
			opPlayer='w';

		return new CheckersState(_tBoard, opPlayer);
	}

	/* Function test if a move is valid by checking if its in the hash map */
	boolean isValid(Move x) {
		if (_board.containsKey(x.start()) || _board.containsKey(x.end()))
			return true;
		else
			return false;		
	}

	/* set the current player of the gamestate */
	void setPlayer(char player) {
		_currentPlayer = player;
	}

	/* return the current player */
	Character player() {
		return _currentPlayer;
	}

	/*
	 * Return an empty board.
	 */
	Map<Integer, Character> emptyBoard() {
		Map<Integer, Character> emptyBoard = baseBoard();
		for (Map.Entry<Integer, Character> entry : emptyBoard.entrySet()) {
			entry.setValue(' ');
		}
		return emptyBoard;
	}

	/* "Internal to Server" board conversion - 
	 * Convert Samuel's checkers representation to 2D array, adherent to the
	 * server represetation with indices zeroed in the "lower-left" corner, 
	 * refer to Samuel, pp. 225
	 */
	char[][] itos() {
		char[][] serverState = new char[8][8];
		// fill array with blank chars
		for (char[] row : serverState) Arrays.fill(row, ' ');
		Point temp;

		for (Map.Entry<Integer, Character> entry : _board.entrySet()) {
			temp = Helper.serverRep(entry.getKey());
			serverState[temp.x][temp.y] = entry.getValue();

		}

		return serverState;
	}

	/* Convert a given board in server form to the internal representation */
	Map<Integer, Character> stoi(char[][] serverBoard) {
		Map<Integer, Character> board = emptyBoard();
		Point temp;

		// use the internal board keys to pull the value of each valid space, one-by-one
		for (Map.Entry<Integer, Character> entry : board.entrySet()) {
			temp = Helper.serverRep(entry.getKey());
			int x = temp.x;
			int y = temp.y;
			entry.setValue(serverBoard[x][y]);
		}
		return board;
	}



	// print out state in the server's/human readable representation.
	void printState() {
		char[][] boardState = itos();
		System.out.println("Black --");
		for (int i = _boardSize - 1; i >= 0; i--) {
			for (int j = 0; j < _boardSize; j++) {
				System.out.print("[" + boardState[j][i] + "]");
			}
			System.out.println();
		}
		System.out.println("White --");
		System.out.println("\n" + _currentPlayer + "'s Move");
		System.out.println("*************************");
	}

	/*
	 * It is terminal when the ply length is met in the tree or when there is no opponent piece
	 */
	boolean isTerminal(int p){
		if ( p== 6 || 
				( (!(_board.containsValue('b')) && !(_board.containsValue('B'))) 
						||  (!(_board.containsValue('w')) && !(_board.containsValue('W'))) ) ){
			return true;
		}
		return false;
	}

	/*
	 * According to Samuel's Paper each piece has a value. King = 1.5 Pawn= 1.0. Utility calculates
	 * how many pieces are on white's board.  
	 */
	double utility(){
		double Utility=0.0;
		double modifier;
		// if W, W's pieces should add to utility. If B, W's pieces should subtract from utility.
		if (_currentPlayer == 'w') modifier = 1.0;
		else modifier = -1.0;

		for (Map.Entry<Integer, Character> entry : _board.entrySet()) {
			Character temp = entry.getValue();
			if (temp == 'w') // if its a pawn the sum is increase by one
				Utility=Utility+(modifier*1.0);
			else if (temp == 'W' ) // if its a king increase by 1.5
				Utility=Utility+(modifier*1.5);
			else if (temp == 'b') 
				Utility=Utility-(modifier*1.0);
			else if (temp == 'B')
				Utility=Utility-(modifier*1.5);
		}

		return Utility;
	}

	double evaluation(){
		double val = 0.0;
		val += 32.0 * CNTR();
		val += 65536.0 * KCENT();
		val += 256.0 * ADV();
		val += 512.0 * CRAMP();
		return val;
	}

    /* return value for if the following squares are occupied by an active man.
     *
     */
    double CENT() {
        int[] test = new int[] {11,12,15,16,20,21,24,25};
        double val = 0;
        for (int i : test) {
            if (Character.toLowerCase(_board.get(i)) == _currentPlayer) {
                val += 1.0;
            }
        }

        return val;
    }

    /* return shortened value for opposing player center control.
     */
    double CNTR() {
        char activePlayer;
        if (_currentPlayer == 'b') activePlayer = 'w';
        else activePlayer = 'b';

        int[] test = new int[] {11,12,15,16,20,21,24,25};
        double val = 0;
        for (int i : test) {
            if (Character.toLowerCase(_board.get(i)) == activePlayer) {
                val++;
            }
        }

        return val;
    }
	
	int KCENT(){
		int val = 0;
		Integer[] positions = {11, 12, 15, 16, 20, 21, 24, 25};
		for(int p : positions){
			if(_board.get(p).equals(Character.toUpperCase(_currentPlayer))){
				val++;
			}
		}
		return val;
	}

	int MOB(){
		int val = 0;
		List<Move> availMoves = null;
		for(Move m : availMoves){
			if(m.jDirection().length() == 0){
				val++;
			}
		}
		return val;
	}

    double CRAMP() {
        int[] test;
        char activePlayer;
        boolean flag = true;
        if (_currentPlayer == 'b') {
            test = new int[] {17,21,22,25};
            //test cramping square
            if (Character.toLowerCase(_board.get(13)) == 'b') {
                // if these nearby values are also occupied
                if (Character.toLowerCase(_board.get(10)) == 'b'
                 || Character.toLowerCase(_board.get(14)) == 'b') {
                    for (int i : test) {
                        if (Character.toLowerCase(_board.get(i)) != 'w') {
                            flag = false;
                        }
                    }

                    if (flag) return 2.0;
                }
            }
        } else {
            test = new int[] {8,11,12,16};
            if (Character.toLowerCase(_board.get(20)) == 'w') {
                if (Character.toLowerCase(_board.get(19)) == 'w'
                 || Character.toLowerCase(_board.get(20)) == 'w') {
                    for (int i : test) {
                        if (Character.toLowerCase(_board.get(i)) != 'b') {
                            flag = false;
                        }
                    }

                    if (flag) return 2.0;
                }
            }
        }

        return 0.0;
    }

	int ADV(){
		int val = 0;
		Integer[] whitePositions = {10, 11, 12, 13, 14, 15, 16, 17};
		Integer[] blackPositions = {19, 20, 21, 22, 23, 24, 25, 26};
		if(_currentPlayer.equals('w')){
			for(int p : whitePositions){
				if(Character.toLowerCase(_board.get(p)) == _currentPlayer){
					val++;
				}
			}
		}else{
			for(int p : blackPositions){
				if(Character.toLowerCase(_board.get(p)) == _currentPlayer){
					val++;
				}
			}
		}
		return val;
	}


	/* return an base starting board in internal representation.
	 */
	static Map<Integer, Character> baseBoard() {
		Map<Integer, Character> baseBoard = new HashMap<Integer, Character>(32);
		ArrayList<Integer> b = new ArrayList<Integer>() {{
			add(1);
			add(2);
			add(3);
			add(4);
			add(5);
			add(6);
			add(7);
			add(8);
			add(10);
			add(11);
			add(12);
			add(13);

		}};

		ArrayList<Integer> w = new ArrayList<Integer>() {{
			add(23);
			add(24);
			add(25);
			add(26);
			add(28);
			add(29);
			add(30);
			add(31);
			add(32);
			add(33);
			add(34);
			add(35);

		}};

		ArrayList<Integer> e = new ArrayList<Integer>() {{
			add(14);
			add(15);
			add(16);
			add(17);
			add(19);
			add(20);
			add(21);
			add(22);

		}};

		for (Integer x : b) {
			baseBoard.put(x, 'b');
		}

		for (Integer x : w) {
			baseBoard.put(x, 'w');
		}

		for (Integer x : e) {
			baseBoard.put(x, ' ');
		}
		return baseBoard;
	}

}