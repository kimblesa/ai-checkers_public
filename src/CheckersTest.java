
public class CheckersTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        // ***NOTE*** the declaration of the array has to be reversed from its appearance here
        // TODO something is wrong with the declaration of this board, avoid for now
        char[][] testBoard = new char[][] { {'w', ' ', 'w', ' ', 'w', ' ', 'w', ' '},   // numbers 32-35
                                            {' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w'},   // numbers 28-31
                                            {'w', ' ', 'w', ' ', 'w', ' ', 'w', ' '},   // numbers 23-26
                                            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},   // numbers 19-22
                                            {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},   // numbers 14-17
                                            {' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b'},   // numbers 10-13
                                            {'b', ' ', 'b', ' ', 'b', ' ', 'b', ' '},   // numbers 5-8
                                            {' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b'} }; // numbers 1-4
		System.out.println("Testing board:");
		CheckersState testState = new CheckersState();
        testState.printState();

		System.out.println("Resulting list:");
//		MiniMaxSearch one=new MiniMaxSearch();
//		Move t =one.minimaxDecision(testState);
		System.out.println(testState.actions());
		//System.out.println(testState.actionsAt(new Move('w', 12, 16)));
        for(Move m : testState.actions()){
            CheckersState y=testState.result(m);
            y.printState();
        }

/*
        Move move = new Move('b', 13, 17);
        // this is broken now :(
        testState = testState.result(move);
        testState.printState();
        move = new Move('w', 26, 21);
        testState = testState.result(move);
        testState.printState();
*/

	}

}
