import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


public class ConsolePlayer implements CheckersPlayer {
	private BufferedReader _in = null;
	public ConsolePlayer(){
		_in = new BufferedReader(new InputStreamReader(System.in));
	}

	@Override
	public String getNextMove(String opponentMove) {
		System.out.print("enter move: ");
		String moveString = null;
		try {
			moveString = _in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return moveString;
	}
	
	@Override
	public String getFirstMove(){
		return "(2:4):(3:5)";
	}
	
	/**
	 * Converts a list of BoardPositions into a string that the server understands.
	 * Not used by ConsolePlayer but might be useful later so leaving it here for now.
	 * @param moveSequence A list of board cells that represent a move sequence such as (2:3)(3:4)
	 */
	public String sendMove(List<BoardPosition> moveSequence){
		StringBuffer move = new StringBuffer();
		for(int i=0; i < moveSequence.size(); i++){
			if(i > 0){
				move.append(":");
			}
			BoardPosition p = moveSequence.get(i);
			move.append("("+p.row+":"+p.col+")");
		}
		return move.toString();	
	}

}
