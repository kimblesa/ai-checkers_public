import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stephen Kimble on 5/4/14.
 */
public class Helper {

    // convert a move from the internal representation to the server representation.
    public static Point serverRep(int intRep) {
        int temp, x, y;

        temp = intRep;
        // change key to a number from 0 to 31
        temp--;
        if (intRep >= 28) temp--;
        if (intRep >= 19) temp--;
        if (intRep >= 10) temp--;

        y = 7-(temp/4);

        if (y % 2 == 1) 	x = (temp % 4) * 2 + 1;
        else				x = (temp % 4) * 2;

        return new Point(x, y);
    }

    // convert a server coordinate to the internal integer representation.
    public static int internalRep(int x, int y) {
        int row = 7 - y;
        int temp = (row * 4) + (x/2);
        temp++;
        if (row >= 2) temp++;
        if (row >= 4) temp++;
        if (row >= 6) temp++;

        return temp;
    }
}
