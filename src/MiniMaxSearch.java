import java.awt.List;
import java.util.Collections;
import java.util.LinkedList;


public class MiniMaxSearch {
	// NOTE: the figure is 5.3 in the 3rd edition of the textbook
	
	/*
	 * minimaxDecision function determines the best move given a state
	 */
	public Move minimaxDecision(CheckersState state){
		Move bestAction = null;
		CheckersState _state= state;
		int phy= 0; /* initialize the ply length to be zero */
		
		/* If there is no move/available actions, then we lose */
		if(_state.actions().size() == 0){
			throw new AssertionError();
		}
		
		/* If the current player is white then we would like to minimize. Player white is second to play. */
		if (state.player()=='w'){
			Double vBest = Double.POSITIVE_INFINITY;
			for (Move s: _state.actions()){
				if(s == null){
					continue;
				}
				// calls the minValue function to start minimax search
				double v = minValue(_state.result(s), phy);
				phy=0;
				if (v < vBest){
					vBest = v;
					bestAction = s;
				}
			}
		}else{
			/* The current player is Black, we should like to maximize. Player Black is First to play. */
			Double vBest = Double.NEGATIVE_INFINITY;
				for (Move s: _state.actions()){ 
					if(s == null){
						continue;
					}
					// calls the maxValue function to begin minimax search  
					double v = maxValue(_state.result(s), phy);
					phy=0;
					if (v > vBest){
						vBest = v;
						bestAction = s;
					}	
				}
		}
		 return bestAction;
	}
	
	//this function minimizes the value by finding the smallest value
	public double minValue(CheckersState state, int p){
		CheckersState _state= state;
		int phyLength = p;
		// checks if the board state is terminal if it is return the utility
		// _state evaluation places more value into utility.
		if (_state.isTerminal(phyLength)){
			return _state.utility() + _state.evaluation();
		}
		else {
			// if the state is not terminal then increase ply length and start predicting opponents moves
			// and calls the MaxValue function. 
			phyLength++;
			double v = Double.POSITIVE_INFINITY;
			for(Move g : _state.actions()){
				v = Math.min(v, maxValue(state.result(g), phyLength) );
			}
			return v;
		}	
	}
	//this function maximizes the value by finding the maximum value
	public double maxValue(CheckersState state, int p){
		CheckersState _state= state;
		int phyLength=p;
		// checks if the board state is terminal if it is return the utility
		// _state evaluation places more value into utility.
		if (_state.isTerminal(phyLength)){
			return _state.utility() + _state.evaluation();
		}
		else {
			// if the state is not terminal then increase ply length and start predicting opponents moves
			// and calls the MinValue function. 
			phyLength++;
			double v = Double.NEGATIVE_INFINITY;
			for(Move g : _state.actions()){
				v = Math.max(v, minValue(state.result(g), phyLength));
			}
			return v;
		}	
	}


}
	
	