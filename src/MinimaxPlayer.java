
public class MinimaxPlayer implements CheckersPlayer {
	
	private CheckersState _gameState;
	private MiniMaxSearch _minimax;
	
	public MinimaxPlayer(Character myColor){
		_gameState = new CheckersState();
		_minimax = new MiniMaxSearch();
	}

	@Override
	public String getNextMove(String opponentMove) {
		//update our game state with the opposing move
		Move theirMove = new Move(_gameState.player(), opponentMove);
		_gameState = _gameState.result(theirMove);
		System.out.println("Before my move:");
		_gameState.printState();
		//find the best move now	
//		System.out.println(_gameState.getHash());
		Move m = _minimax.minimaxDecision(_gameState);
		//update out game state with our move
		_gameState = _gameState.result(m);
		System.out.println("After my move:");
		_gameState.printState();
		return m.serverToString();
	}
	
	@Override
	public String getFirstMove(){
		//we are white, find first move
		System.out.println("Before my move:");
		_gameState.printState();
		Move myMove = _minimax.minimaxDecision(_gameState);
		_gameState = _gameState.result(myMove);
		System.out.println("After my move:");
		_gameState.printState();
		return myMove.serverToString();
	}
	
	public static void main(String[] args){
	}

}
