import java.awt.Point;

public class Move {
	// Move class. this is a structure containing
	// three parameters: player, starting coordinate,
	// ending coordinate.
	
	private Character _player;
	private Integer _start;
	private Integer _end;
	private String _direction="";

	
	public Move(Character p, Integer s, Integer e) {
		_player = p; 
		_start = s;
		_end = e;
	}
	
	public String lastMove(){
		if (_direction.length()==0)
			return "";
		else
		return _direction.substring(_direction.length()-2, _direction.length());
	}

	
	public Move(Character p, Integer s, Integer e, String d) {
		_player = p; 
		_start = s;
		_end = e;
		_direction=_direction+d;
	}
	
	/* This function converts the string given by the server and make it a actual move such that our results function
	 * should process.
	 * TODO This function works however it can be implemented better. Suggestion: look at actions, actionAt
	 * and results() before changing this. Duplicate code must be a better way to implement this
	 */
	public Move(Character player, String serverRep) {
		/*
		 * Given a string eg. "Move:White:(0:0):(2:2):(4:0):(6:2)" converts it to 
		 *  00224062
		 */
		String t =serverRep;
		t=t.replace(t.substring(0, t.indexOf(":")+1), "");
		t=t.replace(t.substring(0, t.indexOf(":")+1), "");
		t= t.replace("(", "");
		t= t.replace(")", "");
		t= t.replace(":", "");

		/*
		 * x1 ="0" x2 = "0" update the string. The string is now 224062
		 */
		String x1= t.substring(0,1);
		t=t.replaceFirst(x1, "");
		String x2= t.substring(0,1);
		t=t.replaceFirst(x2, "");
		
		/*
		 * Parse the string and find its starting key value
		 */
		int repX = Integer.parseInt(x1);
		int repY = Integer.parseInt(x2);
		int startKey = Helper.internalRep(repY, repX);
		int temp = startKey;
		
		/*
		 * A loop that finds the next value and or its jump direction or regular move
		 */
		while (!(t.isEmpty())){
			
			x1= t.substring(0,1);
			t=t.replaceFirst(x1, ""); // grabs first integer
			x2= t.substring(0,1);
			t=t.replaceFirst(x2, ""); // grabs second integer
			
			repX = Integer.parseInt(x1); // converts them to be int
			repY = Integer.parseInt(x2); // converts them to be int
			
			int nextKey = Helper.internalRep(repY, repX); // Uses helper function to convert them to hash map key representation
			
			int left=5, right=4;
			int tempLeft, tempRight, tempLowerLeft, tempLowerRight;
		
			if (player == 'b') {
				tempLeft = temp + left;
				// find the direction of from the start to nextkey 
				if (tempLeft == nextKey) // if its just a regular move 
					_direction=_direction; // _direction should equal ""
				else{
					tempLeft= tempLeft+left; // if its a jump move 
					if (tempLeft == nextKey)
						_direction=_direction+"ul"; // concatenate the jump direction
				}
				
				tempRight = temp + right; 
				if (tempRight == nextKey) // checks if its a regular move
					_direction=_direction;
				else{
					tempRight= tempRight+right;
					if (tempRight == nextKey) // checks if its a jump move
						_direction=_direction+"ur";
				}
				
				tempLowerLeft= temp - right; // same comments as above
				if (tempLowerLeft == nextKey)
					_direction=_direction;
				else{
					tempLowerLeft= tempLowerLeft-right;
					if (tempLowerLeft == nextKey)
						_direction=_direction+"ll";
				}
				
				tempLowerRight= temp - left;
				if (tempLowerRight == nextKey)
					_direction="";
				else{
					tempLowerRight= tempLowerRight-left;
					if (tempLowerRight == nextKey)
						_direction=_direction+"lr";
				}
			} else {
				left=-5;
				right=-4;
				
				tempLeft = temp + left;
				if (tempLeft == nextKey)
					_direction=_direction;
				else{
					tempLeft= tempLeft+left;
					if (tempLeft == nextKey)
						_direction=_direction+"ul";
				}
				
				tempRight = temp + right;
				if (tempRight == nextKey)
					_direction=_direction;
				else{
					tempRight= tempRight+right;
					if (tempRight == nextKey)
						_direction=_direction+"ur";
				}
				
				tempLowerLeft= temp - right;
				if (tempLowerLeft == nextKey)
					_direction=_direction;
				else{
					tempLowerLeft= tempLowerLeft-right;
					if (tempLowerLeft == nextKey)
						_direction=_direction+"ll";
				}
				
				tempLowerRight= temp - left;
				if (tempLowerRight == nextKey)
					_direction=_direction;
				else{
					tempLowerRight= tempLowerRight-left;
					if (tempLowerRight == nextKey)
						_direction=_direction+"lr";
				}
			} 
			temp=nextKey;
		}
		
		_player = player; 
		_start=startKey;
		_end=temp;
		System.out.println("ServerRep: "+serverRep);
	}
	
	String jDirection(){
		return _direction;
	}
	
	Integer start() {
		return _start;
	}
	
	
	Integer end() {
		return _end;
	}
	
	Character player() {
		return _player;
	}
	
	// internal toString, simply printing out the move in Samuel's representation.
	public String toString() {
		return new String(_player + "; " + _start + "->" + _end+ " "+ _direction);
	}
	
	
	/*
	 * Takes our Move and represent it as a move that the server can read. 
	 */
	public String serverToString() {
		Point temp;
		int start= _start, left=-10, right=-8;
		temp = Helper.serverRep(_start); // convert the the key to a point
		String msg = new String("("+ temp.y+":" + temp.x + ")"); 
		String d=_direction;
		int tempLoc;
		
		/* if it is a regular move convert the end to a point and concatenates the string*/
		if (_direction.equals("")){	
			temp = Helper.serverRep(_end);
			msg += new String(":("+ temp.y+ ":"+ temp.x + ")");
			System.out.println("ServerRep: "+ msg);
			return msg;
		}
		else if (_player == 'b' || _player == 'B') { //checks the player to configure the left and right
				 left=10;
				 right=8; 
			}
		/*
		 * Converts the _direction into actual points in the server for representation
		 */
			while (!(d.length()==0)){
				String t=d.substring(0, 2);
				
				if (t.equals("ul")){ // if first two letters in _direction is "ul"
					tempLoc = start+left;
					temp=Helper.serverRep(tempLoc); //represent the key value into a point
					msg += new String(":(" + temp.y + ":" + temp.x + ")"); // concatenate string message
					start=tempLoc; // reset starting point to tempLoc
				}
				else if (t.equals("ur")){
					tempLoc = start+right;
					temp=Helper.serverRep(tempLoc);
					msg += new String(":(" + temp.y + ":" + temp.x + ")");
					start=tempLoc;
				}
				else if (t.equals("ll")){
					tempLoc = start - right;
					temp=Helper.serverRep(tempLoc);
					msg += new String(":(" + temp.y + ":" + temp.x + ")");
					start=tempLoc;
				}
				else {
					tempLoc = start - left;
					temp=Helper.serverRep(tempLoc);
					msg += new String(":(" + temp.y + ":" + temp.x + ")");
					start=tempLoc;
				}
				d=d.replaceFirst(t, ""); // replace string to continue while loop
			}
			
			System.out.println("ServerRep: "+ msg);
			return msg;
	}
		


}
